export enum Permissions {
    DENIED = 'Denied',
    READ = 'Read',
    READ_WRITE = 'ReadWrite',
    ADMIN = 'Admin'
}