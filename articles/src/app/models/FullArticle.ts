import { Permissions } from "./enumerations/Permissions";

export class FullArticle{
    articleId: string;
    title: string;
    author: string;
    text: string;
    isPublic: Boolean;
    brief: string;
    created: string;
    updated: string;
    permission: Permissions;    
}