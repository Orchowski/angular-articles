import { Permissions } from "./enumerations/Permissions";

export class ListedArticle{
    articleId: string;
    title: string;
    brief: string;
    author: string;
    created: string;
    permission: Permissions;
}