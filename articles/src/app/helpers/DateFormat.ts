export class DateFormat {
    private rawDate: any;

    constructor(date: any) {
        this.rawDate = date;
    }

    getFull(): string {
        if (this.rawDate === undefined || isNaN(Date.parse(this.rawDate))) return '';
        return new Date(this.rawDate).toLocaleString();
    }
}