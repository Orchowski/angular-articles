import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';

import { AppComponent } from './app.component';
import { ArticleListComponent } from './components/article-list/article-list.component';
import { createCustomElement } from '@angular/elements'
import { ArticlesService } from './rest-services/articles.service';
import { HttpClientModule } from '@angular/common/http';
import { ArticleDetailsComponent } from './components/article-details/article-details.component';
import { ListedArticleComponent } from './components/listed-article/listed-article.component';
import { CreateArticleComponent } from './components/create-article/create-article.component';
import { EditArticleComponent } from './components/edit-article/edit-article.component';

import { FullArticle } from 'src/app/models/FullArticle';
import * as EventBus from 'eventbusjs';

@NgModule({
  declarations: [
    AppComponent,
    ArticleListComponent,
    ListedArticleComponent,
    CreateArticleComponent,
    ArticleDetailsComponent,
    EditArticleComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  // bootstrap: [AppComponent],
  entryComponents: [    
    ArticleListComponent,
    CreateArticleComponent,
    ArticleDetailsComponent,
    EditArticleComponent
  ],
  providers: [ArticlesService]
})
export class AppModule {
  constructor(private injector: Injector) {
    //  (<any>window).EventBus = EventBus;
  }

  ngDoBootstrap() {
    // const appComponent = createCustomElement(AppComponent, { injector: this.injector });
    const articleListComponent = createCustomElement(ArticleListComponent, { injector: this.injector });
    const articleDetailsComponent = createCustomElement(ArticleDetailsComponent, { injector: this.injector });
    const createArticleComponent = createCustomElement(CreateArticleComponent, { injector: this.injector });
    const editArticleComponent = createCustomElement(EditArticleComponent, { injector: this.injector });
    
    // customElements.define('application-l', appComponent);
    customElements.define('article-list', articleListComponent);
    customElements.define('article-details', articleDetailsComponent);
    customElements.define('create-article', createArticleComponent);
    customElements.define('edit-article', editArticleComponent);
  }
}
