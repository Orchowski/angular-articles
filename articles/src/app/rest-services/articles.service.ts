import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ArticlesService {
  address: string;
  constructor(private http: HttpClient) {

    if ((<any>window).managementAddress === undefined) {
      console.log('managementAddress not set! Please configure it on host. Now location.host used');
      (<any>window).managementAddress = 'http://localhost:8080'//self.location.host;
    }
    if ((<any>window).readToken === undefined) {
      console.log("no read token method");
      (<any>window).readToken = () => {
        return "78a2ce10-1cb3-11e9-954b-effaa8a0cfb8"
      }
    }
    this.address = (<any>window).managementAddress;
    console.log('managementAddress: ' + this.address);

  }

  getAll() {
    return this.http.get(this.address + "/articles/user", this.prepareHeaders());
  }

  getArticle(id: string) {
    var observable =  this.http.get(this.address + "/articles/" + id, this.prepareHeaders());
    return observable.toPromise().catch(this.onUnauthorized);
  }

  postArticle(postBody: { isPublic: boolean; text: string; title: string; }): any {
    var observable =  this.http.post(this.address + "/articles", postBody, this.prepareHeaders());
    return observable.toPromise().catch(this.onUnauthorized);    
  }

  patchArticle(id: string, patchBody: { isPublic: boolean; text: string; title: string; }): any {
    var observable = this.http.patch(this.address + "/articles/" + id, patchBody, this.prepareHeaders());
    return observable.toPromise().catch(this.onUnauthorized);    
  }

  delete(id: string): any {
    var observable = this.http.delete(this.address + "/articles/" + id, this.prepareHeaders());
    return observable.toPromise().catch(this.onUnauthorized);
  }

  private prepareHeaders() {
    var token = (<any>window).readToken();

    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization:  token == null || token == undefined ? '' : token
      })
    };
  }

  onUnauthorized: any = (err) => {
    if (err != null && err.status == 403) {
      (<any>window).EventBus.dispatch("unauthorized");
    }
  }
}