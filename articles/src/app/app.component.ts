import { Component } from '@angular/core';
import * as EventBus from 'eventbusjs';
import { FullArticle } from './models/FullArticle';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  article: FullArticle;
  constructor() {
   
  }
  onSelect() {
    console.log('triggered');
    EventBus.dispatch("refreshArticleList");
  }
}
