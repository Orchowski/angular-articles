import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, NgZone } from '@angular/core';
import { init } from 'pell'
import * as  beautifier from 'js-beautify'
import { FullArticle } from '../../models/FullArticle';
import { ArticleDetailsComponent } from '../article-details/article-details.component';
import { ArticlesService } from 'src/app/rest-services/articles.service';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit, AfterViewInit {
  output: string;
  outputNormal: string;
  title: string;
  isPublic: boolean;

  @ViewChild('editor') editor: ElementRef;
  service: ArticlesService;

  constructor(private _articlesService: ArticlesService, private zone: NgZone) {
    this.service = _articlesService;
    this.isPublic = true;
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    init({
      element: this.editor.nativeElement,
      onChange: html => {
        this.zone.run(() => { this.output = beautifier.html(html); });
        this.onTxtChange(this.output);
        this.outputNormal = html;
      }
    });
  }

  onTitleChange(event: any) {
    this.title = event.target.value;
    (<any>window).EventBus.dispatch("create-article.titleChanged", event);
  }

  onTxtChange(event: any) {
    (<any>window).EventBus.dispatch("create-article.textChanged", event);
  }

  onPublicChange(event: any) {
    this.isPublic = event.target.checked;
  }

  publish() {
    console.log("send");
    if (!(this.outputNormal || this.title)) {
      alert('Article is empty. Please fill form');
      return;
    }
    this.service.postArticle(
      {
        isPublic: this.isPublic,
        text: this.outputNormal,
        title: this.title
      }).then(data => {
        window.alert("Created succesfully");
        (<any>window).EventBus.dispatch("create-article.publishedSuccessfully", data);
      });
  }
}
