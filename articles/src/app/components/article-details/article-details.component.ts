import { Component, OnInit, Input, NgZone } from '@angular/core';
import { Md5 } from 'ts-md5';
import { ArticlesService } from 'src/app/rest-services/articles.service';
import { FullArticle } from 'src/app/models/FullArticle';
import { DateFormat } from 'src/app/helpers/DateFormat';
import { Permissions } from '../../models/enumerations/Permissions';

@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.css']
})
export class ArticleDetailsComponent implements OnInit {
  service: ArticlesService;
  @Input('id-article') articleId: string;
  hash: string;
  article: FullArticle = new FullArticle();
  canEdit: boolean;

  constructor(private _articlesService: ArticlesService, private zone: NgZone) {
    this.service = _articlesService;
  }

  ngOnInit() {
    if (!this.articleId) {
      (<any>window).EventBus.addEventListener("article-details_articleDataLoad", this.articleDataLoadEvent, this);
      return;
    }
    console.log('article: ' + this.articleId);

    var dataObservable = this.service.getArticle(this.articleId);
    dataObservable.catch((e)=>{
      if(e.status == 404){
        (<any>window).EventBus.dispatch("article-details_articleNotFound");
      }
    })

    dataObservable.then((data: any) => {
      var articleResponse = data.data.article;
      this.articleDataLoad(articleResponse);
    });

  }

  articleDataLoadEvent(event: any) {
    console.log(event);
    this.articleDataLoad(event.target);
  }

  onEdit() {
    if ((<any>window).EventBus === undefined) {
      console.log("no event bus registered");
      return;
    }
    (<any>window).EventBus.dispatch("articleEdit", this.article.articleId);
  }

  articleDataLoad(data: any) {
    this.zone.run(() => {
      this.article.articleId = data.articleId;
      this.article.title = data.title;
      this.article.author = data.author;
      this.article.text = data.text;
      this.article.isPublic = data.isPublic == 'true';
      this.article.permission = data.permission;
      this.article.created = new DateFormat(data.created).getFull();
      this.article.updated = new DateFormat(data.updated).getFull();
      this.hash = Md5.hashStr(this.article.author).toString();
      this.canEdit = this.article.permission.valueOf().toLowerCase() == Permissions.ADMIN.valueOf().toLowerCase();
    });
  }
}
