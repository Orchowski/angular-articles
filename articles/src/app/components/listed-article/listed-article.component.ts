import { Component, OnInit, Input } from '@angular/core';
import { ListedArticle } from 'src/app/models/ListedArticle';
import { DateFormat } from 'src/app/helpers/DateFormat';
import { Md5 } from 'ts-md5';

@Component({
  selector: 'listed-article',
  templateUrl: './listed-article.component.html',
  styleUrls: ['./listed-article.component.css']
})

export class ListedArticleComponent implements OnInit {
  @Input() element: ListedArticle;
  @Input() isSelected: boolean;
  hash: string;

  constructor() {
  }

  ngOnInit() {
    this.hash = Md5.hashStr(this.element.author).toString();
    this.element.created = new DateFormat(this.element.created).getFull();
  }
}
