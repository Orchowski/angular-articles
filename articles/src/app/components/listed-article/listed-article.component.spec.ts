import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListedArticleComponent } from './listed-article.component';

describe('ListedArticleComponent', () => {
  let component: ListedArticleComponent;
  let fixture: ComponentFixture<ListedArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListedArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListedArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
