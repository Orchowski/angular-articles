import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, NgZone, Input } from '@angular/core';
import { init } from 'pell'
import * as  beautifier from 'js-beautify'
import { ArticlesService } from 'src/app/rest-services/articles.service';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.css']
})
export class EditArticleComponent implements OnInit, AfterViewInit {
  output: string;
  outputNormal: string;
  title: string;
  isPublic: boolean;
  
  @Input('id-article') articleId: string;  
  @ViewChild('titleInput') titleInput: ElementRef;
  @ViewChild('editor') editor: ElementRef;
  service: ArticlesService;

  constructor(private _articlesService: ArticlesService, private zone: NgZone) {
    this.service = _articlesService;
  }

  ngOnInit() {
    if (!this.articleId) {
      alert("No article provided")
      return;
    }
    console.log('edited article: ' + this.articleId);
    this.service.getArticle(this.articleId).then((data: any) => {
      var articleResponse = data.data.article;
      this.articleDataLoad(articleResponse);
    });
  }


  ngAfterViewInit(): void {
    init({
      element: this.editor.nativeElement,
      onChange: html => {
        this.zone.run(() => { this.output = beautifier.html(html); });
        this.onTxtChange(this.output);
        this.outputNormal = html;
      }
    });
  }

  onTitleChange(event: any) {
    this.title = event.target.value;
    (<any>window).EventBus.dispatch("edit-article.titleChanged", event);
  }

  onTxtChange(event: any) {
    (<any>window).EventBus.dispatch("edit-article.textChanged", event);
  }
  onPublicChange(event: any) {
    this.isPublic = event.target.checked;    
  }

  publish() {
    if (!(this.output || this.title)) {
      alert('Article is empty. Please fill form');
      return;
    }
    this.service.patchArticle(this.articleId,
      {
        isPublic: this.isPublic,
        text: this.output,
        title: this.title
      }).then(data => {
        window.alert("Updated succesfully");
        (<any>window).EventBus.dispatch("edit-article.publishedSuccessfully", data);
      });
  }
  
  delete() {  
    this.service.delete(this.articleId).then(data => {
        window.alert("Deleted succesfully");
        (<any>window).EventBus.dispatch("articleSelected", {articleId: this.articleId});
      });
  }


  articleDataLoad(data: any) {
    this.zone.run(() => {
      this.editor.nativeElement.content.innerHTML = data.text;
      this.titleInput.nativeElement.value = data.title;      
      this.title = data.title;     
      this.output = data.text;
      this.isPublic = data.isPublic;      
    });
  }
}
