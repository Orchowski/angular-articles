import { Component, OnInit, AfterViewInit, Input, NgZone } from '@angular/core';
import { ListedArticle } from 'src/app/models/ListedArticle';
import { ArticlesService } from 'src/app/rest-services/articles.service';

@Component({
  selector: 'article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit, AfterViewInit {
  @Input() height: string = '100%';
  service: ArticlesService;
  articles: ListedArticle[] = [];

  ngAfterViewInit(): void {
  }

  constructor(private _articlesService: ArticlesService, private zone: NgZone) {
    this.service = _articlesService;
    if ((<any>window).EventBus === undefined) {
      console.log("no event bus registered");
      return;
    }
    (<any>window).EventBus.addEventListener("refreshArticleList", this.onEvent, this);
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.service.getAll()
      .subscribe((data: any) => {
        this.zone.run(() => {
          if (data.data.articles == null) { this.articles = [] };
          this.articles = data.data.articles.map(element => {
            let article = new ListedArticle();
            article.articleId = element.articleId;
            article.author = element.author;
            article.permission = element.permission;
            article.title = element.title;
            article.brief = element.brief;
            article.created = element.created;
            if (article.brief.length > 200) {
              article.brief += '...'
            }
            return article;
          });
        });
      });
  }

  onEvent(event: any) {
    console.log("event handled=" + event.type);
    this.loadData();
  }

  onSelect(selected: ListedArticle) {
    console.log("article selected=" + selected.articleId);
    if ((<any>window).EventBus === undefined) {
      console.log("no event bus registered");
      return;
    }
    (<any>window).EventBus.dispatch("articleSelected", selected);
  }
}
