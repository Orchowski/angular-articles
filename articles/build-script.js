const fs = require('fs-extra');
const concat = require('concat');


(async function build() {
    const files = [
        './dist/articles/runtime.js',
        './dist/articles/polyfills.js',
        './dist/articles/main.js'
    ]

    await fs.ensureDir('elements');
    await concat(files,'elements/article-list.js')
    
})()