import Vue from 'vue'
import App from './App.vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import EventBus from 'eventbusjs';
import router from './router'
import listener from './eventListener'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import * as authService from './services/authorizationService';

function configureBus() {
  window.EventBus = EventBus;
  window.readToken = authService.readToken;
};

function configureManagementService() {
  window.managementAddress = '{MANAGEMENT_URI}';
  var imported = document.createElement('script');
  imported.src = window.managementAddress + '/article-list.js';
  document.head.appendChild(imported);
};

function configureUsersService() {
  window.usersAddress = '{USERS_URI}';
  authService.verifyAuth();
};

Vue.config.productionTip = false
configureManagementService();
configureUsersService();
configureBus();
listener();

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
