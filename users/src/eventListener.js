import router from './router'
import * as authSrv  from "./services/authorizationService.js";
const add = () => {    
    articleSelectedListener();
    unauthorizedListener();
    articleEditListener();
    articleNotFound();
}

function articleSelectedListener() {
    window.EventBus.addEventListener("articleSelected", (e) => {
        var selectedId = e.target.articleId;
        router.push({ name: 'article', params: { articleId: selectedId } });
    }, this);
}


function articleNotFound() {
    window.EventBus.addEventListener("article-details_articleNotFound", () => {        
        router.push({ name: 'main-articles'});
    }, this);
}

function articleEditListener() {
    window.EventBus.addEventListener("articleEdit", (e) => {
        var selectedId = e.target;
        router.push({ name: 'edit', params: { articleId: selectedId } });
    }, this);
}

function unauthorizedListener() {
    window.EventBus.addEventListener("unauthorized", (e) => {
        authSrv.logout();
    }, this);
}

export default add;