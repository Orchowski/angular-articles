export function loginWithEmailAndPassword(email, password) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", window.usersAddress + "/users/login", false);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(JSON.stringify({ email: email, password: password }));
    return xhttp.response;
}

export function registerUser(userData) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", window.usersAddress + "/users/register", false);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(JSON.stringify(userData));
    return { isSuccessfull: xhttp.status == 204, status: xhttp.status, response: xhttp.response };
}

export function verify(tokenToVerify) {
    if (tokenToVerify) {
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", window.usersAddress + "/verify/token", false);
        xhttp.setRequestHeader("Content-type", "application/json");
        let data = { token: tokenToVerify };
        xhttp.send(JSON.stringify(data));
        if (xhttp.response.fault) {
            return false;
        }
        return JSON.parse(xhttp.response);        
    }
    return false;
}