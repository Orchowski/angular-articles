import * as usersService from './usersService';
import router from '../router'

const TOKEN_PARAM = 'gkhsiuyae984yiuhvisyfoa8ufvri8ya9nryv9';
const USER_INFO_PARAM = 's;0f-fpsi9se,9-ise9-rgi9s-reigs90dfgis90dfg';

export function authorize(data) {    
        localStorage.setItem(TOKEN_PARAM, data);
        verifyAuth();    
}

export function readToken() {
    return localStorage.getItem(TOKEN_PARAM);
}

export function readUserData() {
    return JSON.parse(localStorage.getItem(USER_INFO_PARAM));
}

export function logout() {
     localStorage.removeItem(TOKEN_PARAM);
     localStorage.removeItem(USER_INFO_PARAM);
     router.push('/login')
}

export function verifyAuth() {
    var verification = usersService.verify(localStorage.getItem(TOKEN_PARAM));
    if (!verification.fault) {        
        localStorage.setItem(USER_INFO_PARAM, JSON.stringify(verification));
    } else {
        logout();
    }
}