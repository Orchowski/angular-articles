import Vue from 'vue'
import Router from 'vue-router'
import ArticleList from "./components/management/ArticleList";
import CreateArticle from "./components/management/CreateArticle";
import ArticleDetails from "./components/management/ArticleDetails";
import ArticleEdit from "./components/management/ArticleEdit";
import Login from "./components/self/Login";
import Register from "./components/self/Register";
import * as authSrv from "./services/authorizationService";

Vue.use(Router);

const router = new Router(
    {
        routes: [
            {
                path: '/',
                name: 'main-articles',
                component: ArticleList
            },
            {
                path: '/login',
                name: 'login',
                component: Login,
                meta: {
                    requiresGuest: true
                }
            },
            {
                path: '/register',
                name: 'register',
                component: Register,
                meta: {
                    requiresGuest: true
                }
            },
            {
                path: '/create',
                name: 'create',
                component: CreateArticle,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: '/article/:articleId',
                name: 'article',
                component: ArticleDetails
            },
            {
                path: '/edit/:articleId',
                name: 'edit',
                component: ArticleEdit, meta: {
                    requiresAuth: true
                }
            }
        ]
    }
);


//nav guards
router.beforeEach((to, from, next) => {
    //chek for required
    if (to.matched.some(record => record.meta.requiresAuth)) {
        //check if NOT logged in
        if (!authSrv.readToken()) {
            //go to login page
            next({
                path: '/login',
                query: {
                    redirect: to.fullPath
                }
            });
        } else {
            //Proceed to the route
            next();
        }
    } else if (to.matched.some(record => record.meta.requiresGuest)) {
        //check if logged in
        if (authSrv.readToken()) {
            //go to main page
            next({
                path: '/',
                query: {
                    redirect: to.fullPath
                }
            });
        } else {
            //Proceed to the route
            next();
        }
    } else {
        //Proceed to the route
        next();
    }
});

export default router;