if [[ -z "${USERS_URI}" ]]; then
  USERS_URI=http://localhost:3000
fi
if [[ -z "${MANAGEMENT_URI}" ]]; then
  MANAGEMENT_URI=http://localhost:8080
fi

sed -i 's+{MANAGEMENT_URI}+'"$MANAGEMENT_URI"'+g' ./src/main.js
sed -i 's+{USERS_URI}+'"$USERS_URI"'+g' ./src/main.js